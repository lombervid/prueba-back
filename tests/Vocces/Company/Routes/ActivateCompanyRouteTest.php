<?php

namespace Tests\Vocces\Company\Routes;

use Tests\TestCase;
use Vocces\Company\Domain\ValueObject\CompanyId;
use Vocces\Company\Domain\ValueObject\CompanyStatus;
use Vocces\Company\Infrastructure\CompanyRepositoryEloquent;

class ActivateCompanyRouteTest extends TestCase
{
    private CompanyRepositoryEloquent $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new CompanyRepositoryEloquent();
    }

    /**
     * @test
     * @group route
     * @group access-interface
     * @depends Tests\Vocces\Company\Routes\CreateNewCompanyRouteTest::postCreateNewCompanyRoute
     */
    public function patchActivateCompanyRoute($id)
    {
        $companyId = new CompanyId($id);

        $company = $this->repository->find($companyId);
        $this->assertEquals($company->status(), CompanyStatus::disabled());

        $response = $this->patchJson("/api/company/{$id}/activate");
        $response->assertStatus(200);

        $company = $this->repository->find($companyId);
        $this->assertEquals($company->status(), CompanyStatus::enabled());
    }
}
