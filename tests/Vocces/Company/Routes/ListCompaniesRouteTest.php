<?php

namespace Tests\Vocces\Company\Routes;

use Tests\TestCase;
use Vocces\Company\Infrastructure\CompanyRepositoryEloquent;

class ListCompaniesRouteTest extends TestCase
{
    private CompanyRepositoryEloquent $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new CompanyRepositoryEloquent();
    }

    /**
     * @test
     * @group route
     * @group access-interface
     */
    public function getListCompaniesRoute()
    {
        $response = $this->getJson("/api/companies");
        $response->assertStatus(200)
            ->assertJsonStructure([
                '*' => [
                    'id',
                    'name',
                    'email',
                    'address',
                    'status',
                ]
            ]);
    }
}
