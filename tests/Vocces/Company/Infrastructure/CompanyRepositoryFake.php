<?php

namespace Tests\Vocces\Company\Infrastructure;

use Vocces\Company\Domain\Company;
use Vocces\Company\Domain\CompanyRepositoryInterface;
use Vocces\Company\Domain\ValueObject\CompanyId;

class CompanyRepositoryFake implements CompanyRepositoryInterface
{
    public bool $callMethodFind = false;
    public bool $callMethodFindAll = false;
    public bool $callMethodCreate = false;
    public bool $callMethodActivate = false;

    /**
     * @inheritdoc
     */
    public function find(CompanyId $id): ?Company
    {
        $this->callMethodFind = true;

        return null;
    }

    /**
     * @inheritdoc
     */
    public function findAll(): array
    {
        $this->callMethodFindAll = true;

        return [];
    }

    /**
     * @inheritdoc
     */
    public function create(Company $company): ?Company
    {
        $this->callMethodCreate = true;

        return $company;
    }

    /**
     * @inheritdoc
     */
    public function activate(Company $company): bool
    {
        $this->callMethodActivate = true;

        return true;
    }
}
