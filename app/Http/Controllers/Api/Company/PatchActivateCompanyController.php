<?php

namespace App\Http\Controllers\Api\Company;

use App\Http\Controllers\Controller;
use App\Http\Requests\Company\ActivateCompanyRequest;
use Vocces\Company\Application\CompanyActivator;

class PatchActivateCompanyController extends Controller
{
    public function __invoke(
        ActivateCompanyRequest $request,
        CompanyActivator $service,
        string $id
    ) {
        $service->handle($id);

        return response(['message' => 'Company activated.']);
    }
}
