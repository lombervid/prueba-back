<?php

namespace App\Http\Controllers\Api\Company;

use App\Http\Controllers\Controller;
use Vocces\Company\Application\CompanyLister;

class GetCompanyListController extends Controller
{
    public function __invoke(CompanyLister $service)
    {
        $companies = $service->handle();

        return response()->json(array_map(fn($company) => $company->toArray(), $companies));
    }
}
