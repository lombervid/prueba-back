<?php

namespace Vocces\Company\Infrastructure;

use App\Models\Company as ModelsCompany;
use Vocces\Company\Domain\Company;
use Vocces\Company\Domain\CompanyRepositoryInterface;
use Vocces\Company\Domain\ValueObject\CompanyId;
use Vocces\Company\Domain\ValueObject\CompanyName;
use Vocces\Company\Domain\ValueObject\CompanyEmail;
use Vocces\Company\Domain\ValueObject\CompanyAddress;
use Vocces\Company\Domain\ValueObject\CompanyStatus;

class CompanyRepositoryEloquent implements CompanyRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function find(CompanyId $id): ?Company
    {
        if (!$model = ModelsCompany::find($id)) {
            return null;
        }

        return $this->fromEloquentModel($model);
    }

    /**
     * @inheritDoc
     */
    public function findAll(): array
    {
        $companies = [];

        foreach (ModelsCompany::all() as $model) {
            $companies[] = $this->fromEloquentModel($model);
        }

        return $companies;
    }

    /**
     * @inheritDoc
     */
    public function create(Company $company): ?Company
    {
        $model = ModelsCompany::Create([
            'id'      => $company->id(),
            'name'    => $company->name(),
            'email'   => $company->email(),
            'address' => $company->address(),
            'status'  => $company->status()->code(),
        ]);

        if (!$model) {
            return null;
        }

        return $this->fromEloquentModel($model);
    }

    /**
     * @inheritDoc
     */
    public function activate(Company $company): bool
    {
        return ModelsCompany::where('id', $company->id())
            ->update(['status' => CompanyStatus::enabled()->code()]);
    }

    public function fromEloquentModel(ModelsCompany $model): Company
    {
        return new Company(
            new CompanyId($model->id),
            new CompanyName($model->name),
            new CompanyEmail($model->email),
            new CompanyAddress($model->address),
            new CompanyStatus($model->status)
        );
    }
}
