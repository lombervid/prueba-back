<?php

namespace Vocces\Company\Application;

use Vocces\Shared\Domain\Interfaces\ServiceInterface;
use Vocces\Company\Domain\CompanyRepositoryInterface;
use Vocces\Company\Domain\ValueObject\CompanyId;

class CompanyActivator implements ServiceInterface
{
    /**
     * @var CompanyRepositoryInterface $repository
     */
    private CompanyRepositoryInterface $repository;

    public function __construct(CompanyRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Change company status to active
     *
     * @param string $id
     *
     * @return bool|null
     */
    public function handle(string $id): ?bool
    {
        if (!$company = $this->repository->find(new CompanyId($id))) {
            return null;
        }

        return $this->repository->activate($company);
    }
}
