<?php

namespace Vocces\Company\Domain;

use Vocces\Company\Domain\ValueObject\CompanyId;

interface CompanyRepositoryInterface
{
    /**
     * Find a company by ID
     *
     * @param string $id
     *
     * @return Company|null
     */
    public function find(CompanyId $id): ?Company;

    /**
     * Find a company by ID
     *
     * @return Company[]
     */
    public function findAll(): array;

    /**
     * Persist a new company instance
     *
     * @param Company $company
     *
     * @return Company|null
     */
    public function create(Company $company): ?Company;

    /**
     * Set company status to active
     *
     * @param Company $company
     *
     * @return bool
     */
    public function activate(Company $company): bool;
}
